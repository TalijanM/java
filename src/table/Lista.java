package table;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

public class Lista implements Serializable {
    private List<Student> listaStudenata;
    
    public Lista(){
        this.listaStudenata = new ArrayList();
    }
    public void dodajStudenta(Student s){
        this.listaStudenata.add(s);
    }
    public String [][] prikaz(){
        
        String podaci [][] = new String [this.listaStudenata.size()][4];
        
        Iterator<Student> iter = this.listaStudenata.iterator();
        
        int brojac = 0;
        
        while(iter.hasNext()){
            
            String student [] = new String[4];
            Student s = iter.next();
            
            student[0] = s.getId();
            student[1] = s.getIme();
            student[2] = s.getPrezime();
            student[3] = s.getBrojIndeksa();
            
            podaci[brojac] = student;
            brojac++;
        }
        
        return podaci;
    }
    public void prikaziUTabeli(JTable jtb){
        
        String kolone[] = {"ID", "IME", "PREZIME", "BROJ INDEKSA"};
        
        jtb.setModel(new javax.swing.table.DefaultTableModel(prikaz(), kolone));
    }
    public void ocistiInpute(JTextField jtf, JTextField jtf2, JTextField jtf3, JTextField jtf4){
        jtf.setText("");
        jtf2.setText("");
        jtf3.setText("");
        jtf4.setText("");
    }
    public void sacuvajUFajl(){
        try {
            FileOutputStream fos = new FileOutputStream("Studenti.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this.listaStudenata);
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void procitajIzFajla(){
        try {
            FileInputStream fis = new FileInputStream("Studenti.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.listaStudenata = (List<Student>) ois.readObject();
            fis.close();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    
    // prikaz podataka u textfield
    
    public void prikaziUTextfield(JTable jtb, JTextField idTxt, JTextField imeTxt, JTextField prezimeTxt, JTextField brojIndeksaTxt){
        int red = jtb.getSelectedRow();
        
        idTxt.setText(jtb.getValueAt(red, 0).toString());
        imeTxt.setText(jtb.getValueAt(red, 1).toString());
        prezimeTxt.setText(jtb.getValueAt(red, 2).toString());
        brojIndeksaTxt.setText(jtb.getValueAt(red, 3).toString());
    }
    // izmena podataka u tabeli
    public void izmenaPodataka(JTable jtb, String id, String ime, String prezime, String brojIndeksa){
        Iterator <Student> iter = listaStudenata.iterator();
        int red = jtb.getSelectedRow();
        
        while(iter.hasNext()){
            
            Student s = iter.next();
            if(red >= 0){
                
                this.listaStudenata.get(red).setId(id);
                this.listaStudenata.get(red).setIme(ime);
                this.listaStudenata.get(red).setPrezime(prezime);
                this.listaStudenata.get(red).setBrojIndeksa(brojIndeksa);
                
                prikaziUTabeli(jtb);
                sacuvajUFajl();
            }  
        }
    }
    public void izmenaPodataka2(JTable jtb, JTextField idTxt, JTextField imeTxt, JTextField prezimeTxt, JTextField brojIndeksaTxt){
        izmenaPodataka(jtb, idTxt.getText(), imeTxt.getText(), prezimeTxt.getText(), brojIndeksaTxt.getText());
        
    }
    //brisanje podataka iz tabele
    public void obrisi(JTable jtb, String imeTxt){
        Iterator<Student> iter = this.listaStudenata.iterator(); 
        
        Student s;
        while(iter.hasNext()){
            s = iter.next();
            
            if(s.getIme().equals(imeTxt)){
                
                iter.remove();
                prikaziUTabeli(jtb);
        }
    }
    }
        public void obrisi2(JTable jtb, JTextField imeTxt){
            
           obrisi(jtb, imeTxt.getText());
        }
}

